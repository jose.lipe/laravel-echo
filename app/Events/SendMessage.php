<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 04/01/18
 * Time: 22:13
 */

namespace App\Events;


use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class SendMessage implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * @var Message
     */
    public $message;
    public $user;

    /**
     * SendMessage constructor.
     * @param $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->user = \Auth::user();
    }


    /**
     * @return Channel
     */
    public function broadcastOn()
    {
        // TODO: Implement broadcastOn() method.
        return new PresenceChannel("room.{$this->message->room_id}");
    }
}